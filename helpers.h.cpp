﻿// helpers.h.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <statehelpers.h>

int Math(int a, int b)
{
	return (a + b) * (a + b);
}

int main()
{
	std::cout << Math(2, 5) << std::endl;
	return 0;
}